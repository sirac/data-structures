/*
 * crc.c
 *
 *  Created on: 26/03/2020 15:56:20
 *      Author: j.imbert
 */

 //##############################################################################
 //                        INCLUSIONS
 //##############################################################################
#include "crc.h"
//-----------------------------------------------------------------------------
#include <stdint.h>
//##############################################################################
//                 PRIVATE SYMBOLIC CONSTANTS and MACROS
//##############################################################################
/*
 * Derive parameters from the standard-specific parameters in crc.h.
 */
#define WIDTH    (8 * sizeof(crc))
#define TOPBIT   ((crc)1 << (WIDTH - 1))

#if (REFLECT_DATA == 1)
#undef  REFLECT_DATA
#define REFLECT_DATA(X)			((unsigned char) _Reflect((X), 8))
#else
#undef  REFLECT_DATA
#define REFLECT_DATA(X)			(X)
#endif

#if (REFLECT_REMAINDER == 1)
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)	((crc) _Reflect((X), WIDTH))
#else
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)	(X)
#endif

//----------------------------------------------------------------------------


//##############################################################################
//               PRIVATE TYPES, STRUCTURES, UNIONS and ENUMS
//##############################################################################

//----------------------------------------------------------------------------

//##############################################################################
//                    PRIVATE VARIABLES DEFINITION
//##############################################################################

//##############################################################################
//                      PRIVATE FUNCTION PROTOTYPE
//##############################################################################
uint32_t _LitToBigEndian(uint32_t x);
uint32_t _Reflect(uint32_t data, uint8_t nBits);

/*********************************************************************
 *
 * Function:    crcSlow()
 *
 * Description: Compute the CRC of a given message.
 *
 * Notes:
 *
 * Returns:		The CRC of the message.
 *
 *********************************************************************/
uint32_t CrcSlow(uint8_t const message[], uint32_t nBytes){
	crc            remainder = INITIAL_REMAINDER;
	uint32_t       byte;
	uint8_t		   bit;

	/*
	 * Perform modulo-2 division, a byte at a time.
	 */
	for (byte = 0; byte < nBytes; ++byte){
		/*
		 * Bring the next byte into the remainder.
		 */
		remainder ^= ((crc)REFLECT_DATA(message[byte]) << (WIDTH - 8));

		/*
		 * Perform modulo-2 division, a bit at a time.
		 */
		for(bit = 8; bit > 0; --bit){
			/*
			 * Try to divide the current data bit.
			 */
			if((crc)remainder & TOPBIT){
				remainder = (remainder << 1) ^ POLYNOMIAL;
			}
			else{
				remainder = (remainder << 1);
			}
		}
	}

	/*
	 * The final remainder is the CRC result.
	 */
	return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);

}   /* crcSlow() */

uint32_t _Reflect(uint32_t data, uint8_t nBits){
	uint32_t reflection = 0x00000000;
	uint8_t  bit;

	/*
	 * Reflect the data about the center bit.
	 */
	for (bit = 0; bit < nBits; ++bit){
		/*
		 * If the LSB bit is set, set the reflection of it.
		 */
		if (data & 0x01){
			reflection |= (1 << ((nBits - 1) - bit));
		}

		data = (data >> 1);
	}

	return (reflection);

}	/* reflect() */

uint32_t _LitToBigEndian(uint32_t x){
	return (((x >> 24) & 0x000000ff) | ((x >> 8) & 0x0000ff00) | ((x << 8) & 0x00ff0000) | ((x << 24) & 0xff000000));
}
