/*
 * hash.c
 *
 *  Created on: 01/09/2022
 *      Author: Jordan IMBERT
 */

/*
    Simple implementation of an hash table. This is not usefull for embedded system
    as the size should be 3 times more the number of items to prevent collisions and
    an hashtable is better than liked list for big amount of items.
    This example is not very optimised because the use of all the data of items to
    calculate a key and not support collisions.
    The key is generated with CRC32 as it is available with hardware peripheral on
    MCU like on STM32.

    To improve :
     - Add linked list on each "node" to handle collisions
     - Store the key on the item
     - Use less data to generate the key
     - Add more hash function
     - Add function to search, remove etc
*/

//##############################################################################
//                        INCLUSIONS
//##############################################################################
#include "hash.h"
//----------------------------------------------------------------------------
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
//----------------------------------------------------------------------------

///----------------------------------------------------------------------------

//##############################################################################
//                 EXTERNAL variables, functions
//##############################################################################

//----------------------------------------------------------------------------

//##############################################################################
//                 PRIVATE SYMBOLIC CONSTANTS and MACROS
//##############################################################################

//----------------------------------------------------------------------------
//##############################################################################
//               PRIVATE TYPES, STRUCTURES, UNIONS and ENUMS
//##############################################################################

//----------------------------------------------------------------------------

//##############################################################################
//                    PRIVATE VARIABLES DEFINITION
//##############################################################################

//##############################################################################
//                      PRIVATE FUNCTION PROTOTYPE
//##############################################################################
uint32_t _HashTable_getValidLenght(uint32_t lenght);
bool     _HashTable_isPrime(uint32_t number);
//##############################################################################
//                       PUBLIC FUNCTION CODE
//##############################################################################
/**
 *  After the creation, the lenght of the table will differ of the origin
 *  because of the need of prime number
 */
HashTable_t *HashTable_new(uint32_t lenght, size_t itemSize, crc32FctPtr_t crc32Callback){
    assert(crc32Callback != NULL);

    HashTable_t *table = (HashTable_t *)malloc(sizeof(HashTable_t));
    if(table == NULL){
        return NULL;
    }

    if(HashTable_ctor(table, lenght, itemSize, crc32Callback) < 0){
        free(table);
        return(NULL);
    }

    return(table);
}

int32_t HashTable_ctor(HashTable_t *table, uint32_t lenght, size_t itemSize, crc32FctPtr_t crc32Callback){
    assert(table != NULL);
    assert(crc32Callback != NULL);

    table->crc32Callback = crc32Callback;
    table->lenght = _HashTable_getValidLenght(lenght);
    table->itemSize = itemSize;
    table->items = malloc(table->lenght*table->itemSize);
    if(table->items == NULL){
        return -1;
    }

    return(0);
}

void HashTable_add(HashTable_t *table, void *item){
    assert(item);

    uint32_t id = HashTable_hashWithMod(table, item);
    uint8_t *dest = (uint8_t *)table->items + id*table->itemSize;
    uint8_t *src = item;

    memcpy(dest, src, table->itemSize);
}

//To improve an hash spread uniformly, tableSize should be a prime number
//and not to close of a power of 2 
uint32_t HashTable_hashWithMod(HashTable_t *table, uint8_t *data){
    //Get a unique id of the content of an item
    uint32_t crc32 = table->crc32Callback(data, table->itemSize);
    printf("%s : %u=>%u\r\n", (char *)((char *)data + 4), crc32, crc32  % table->lenght);
    //The hash is only a modulo
    return (crc32  % table->lenght);
}

bool _HashTable_isPrime(uint32_t number){

    unsigned int div = 3;

    while (div * div < number && number % div != 0)
        div += 2;

    return number % div != 0;
}

uint32_t _HashTable_getValidLenght(uint32_t lenght){
    //Make it odd
    lenght *= HASH_TABLE_LENGHT_MARGIN_FACTOR;
    lenght |= 1;

    while(!_HashTable_isPrime(lenght)){
        lenght += 2;
    }

    return(lenght);
}