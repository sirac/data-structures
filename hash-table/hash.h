/*
 * action.h
 *
 *  Created on: 01/09/2022
 *      Author: Jordan IMBERT
 */

#ifndef _HASH_H_
#define _HASH_H_

//#############################################################################
//                        INCLUDES
//#############################################################################
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
//----------------------------------------------------------------------------

//##############################################################################
//                 EXTERNAL variables, functions
//##############################################################################

//#############################################################################
//               PUBLIC SYMBOLIC CONSTANTS and MACROS
//#############################################################################
//The size of the table should be at least 3 times bigger than the number of 
//items. Otherwise, collisions will occure
#define HASH_TABLE_LENGHT_MARGIN_FACTOR  3
//-----------------------------------------------------------------------------

//#############################################################################
//            PUBLIC TYPES, STRUCTURES, UNIONS and ENUMS
//#############################################################################
typedef uint32_t (*crc32FctPtr_t)(uint8_t const *buffer, uint32_t len);
typedef struct{
    uint32_t lenght;
    size_t itemSize;
    crc32FctPtr_t crc32Callback;
    void **items;
}HashTable_t;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//#############################################################################
//              PUBLIC VARIABLES DECLARATION
//#############################################################################

//-----------------------------------------------------------------------------

//#############################################################################
//                     PUBLIC FUNCTION PROTOTYPES
//#############################################################################
HashTable_t *HashTable_new(uint32_t lenght, size_t itemSize, crc32FctPtr_t crc32Callback);
int32_t     HashTable_ctor(HashTable_t *table, uint32_t lenght, size_t itemSize, crc32FctPtr_t crc32Callback);
uint32_t    HashTable_hashWithMod(HashTable_t *table, uint8_t *data);
void        HashTable_add(HashTable_t *table, void *item);
#endif //_HASH_H_
