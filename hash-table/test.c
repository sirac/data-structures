/*
 * test.c
 *
 *  Created on: 02/09/2022
 *      Author: Jordan IMBERT
 */

//##############################################################################
//                        INCLUSIONS
//##############################################################################

#include "hash.h"
#include "crc.h"
//----------------------------------------------------------------------------
#include <stdio.h>
///----------------------------------------------------------------------------

//##############################################################################
//                 EXTERNAL variables, functions
//##############################################################################

//----------------------------------------------------------------------------

//##############################################################################
//                 PRIVATE SYMBOLIC CONSTANTS and MACROS
//##############################################################################

//----------------------------------------------------------------------------
//##############################################################################
//               PRIVATE TYPES, STRUCTURES, UNIONS and ENUMS
//##############################################################################
typedef struct{
    uint8_t age;
    uint16_t size;
    char name[32];
}Person_t;
//----------------------------------------------------------------------------

//##############################################################################
//                    PRIVATE VARIABLES DEFINITION
//##############################################################################

//##############################################################################
//                      PRIVATE FUNCTION PROTOTYPE
//##############################################################################
void printTable(HashTable_t *table);

//##############################################################################
//                       PUBLIC FUNCTION CODE
//##############################################################################
int main(int argc, char *argv[]){

    printf("Debut program \n");
    
    HashTable_t *table = HashTable_new(128, sizeof(Person_t), &CrcSlow);
    Person_t person1 = {10, 145, "Noel"};
    Person_t person2 = {20, 185, "Bernard"};
    Person_t person3 = {30, 175, "Nolwenn"};
    Person_t person4 = {40, 165, "Pierre"};
    Person_t person5 = {50, 168, "JF"};

    HashTable_add(table, &person1);
    HashTable_add(table, &person2);
    HashTable_add(table, &person3);
    HashTable_add(table, &person4);
    HashTable_add(table, &person5);
    
    printTable(table);

    getchar();
    return (0);
}

void printTable(HashTable_t *table){
    for(uint32_t i = 0;i<table->lenght;i++){
        Person_t *p = (Person_t *)((uint8_t *)table->items + i*table->itemSize);
        printf("%s a %dans et mesure %dcm\r\n", p->name, p->age, p->size);
    }
}