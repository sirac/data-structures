/*
 * crc.h
 *
 *  Created on: 02/09/2022
 *      Author: Jordan IMBERT
 */

#ifndef _CRC_H_
#define _CRC_H_

//#############################################################################
//                        INCLUDES
//#############################################################################
#include <stdint.h>
//----------------------------------------------------------------------------

//##############################################################################
//                 EXTERNAL variables, functions
//##############################################################################

//#############################################################################
//               PUBLIC SYMBOLIC CONSTANTS and MACROS
//#############################################################################
#define CRC32


#if defined(CRC_CCITT)

	typedef unsigned short  crc;

    #define CRC_NAME			"CRC-CCITT"
    #define POLYNOMIAL			0x1021
    #define INITIAL_REMAINDER	0xFFFF
    #define FINAL_XOR_VALUE		0x0000
    #define REFLECT_DATA		FALSE
    #define REFLECT_REMAINDER	FALSE
    #define CHECK_VALUE			0x29B1

#elif defined(CRC16)

	typedef unsigned short  crc;

    #define CRC_NAME			"CRC-16"
    #define POLYNOMIAL			0x8005
    #define INITIAL_REMAINDER	0x0000
    #define FINAL_XOR_VALUE		0x0000
    #define REFLECT_DATA		TRUE
    #define REFLECT_REMAINDER	TRUE
    #define CHECK_VALUE			0xBB3D

#elif defined(CRC32)
    #define CRC_NAME			"CRC-32"
    #define POLYNOMIAL			0x04C11DB7
    #define INITIAL_REMAINDER	0xFFFFFFFF
    #define FINAL_XOR_VALUE		0x00000000
    #define REFLECT_DATA		0
    #define REFLECT_REMAINDER	0
    #define CHECK_VALUE			0x0376E6E7

#else
    #error "One of CRC_CCITT, CRC16, or CRC32 must be #define'd."
#endif

//-----------------------------------------------------------------------------

//#############################################################################
//            PUBLIC TYPES, STRUCTURES, UNIONS and ENUMS
//#############################################################################

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

//#############################################################################
//              PUBLIC VARIABLES DECLARATION
//#############################################################################
#if defined(CRC_CCITT)
    typedef unsigned short  crc;
#elif defined(CRC16)
    typedef unsigned short  crc;
#elif defined(CRC32)
    typedef unsigned long  crc;
#endif
//-----------------------------------------------------------------------------

//#############################################################################
//                     PUBLIC FUNCTION PROTOTYPES
//#############################################################################
uint32_t CrcSlow(uint8_t const message[], uint32_t nBytes);



#endif //_CRC_H_
